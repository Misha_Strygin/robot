package ru.smo;


public class Robot {
    private int x;
    private int y;
    private Direction direction;

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Robot() {
        this(0, 0, Direction.UP);
    }

    public String toString() {
        return "Robot{" + " x = " + x + " y = " + y + " Direction = " + direction + '}';
    }

    public void toRotationRight() {
        switch (direction) {
            case UP:
                direction = Direction.RIGHT;
                System.out.println("Я смотрю направо");
                break;
            case RIGHT:
                direction = Direction.DOWN;
                System.out.println("Я смотрю на низ ");
                break;
            case DOWN:
                direction = Direction.LEFT;
                System.out.println("Я смотрю налево");
                break;
            case LEFT:
                direction = Direction.UP;
                System.out.println("Я смотрю на верх");
                break;
            default:
                System.out.println("Извини но янезнаю куда я смотрю");
                break;
        }
    }

    public void toRotationLeft() {
        switch (direction) {
            case UP:
                direction = Direction.LEFT;
                break;
            case LEFT:
                direction = Direction.DOWN;
                break;

            case DOWN:
                direction = Direction.RIGHT;
                break;

            case RIGHT:
                direction = Direction.UP;
                break;
        }
    }


    public void oneStep() {
        switch (direction) {
            case UP:
                y++;
                System.out.println("Я сделал шаг вверх");
                break;
            case RIGHT:
                x++;
                System.out.println("Я сделал шаг на вправо");
                break;
            case DOWN:
                y--;
                System.out.println("Я сделал шаг вниз");
                break;
            case LEFT:
                x--;
                System.out.println("Я сделал шаг налево");
                break;
            default:
                System.out.println("Что-то незнаю куда я сделал шаг)");
                break;


        }
    }

    public void toMove(int x1, int y1) {
        if (x < x1) {
            while (direction != Direction.RIGHT) {
                toRotationRight();
            }
        } else if (x > x1) {
            while (direction != Direction.LEFT) {
                toRotationLeft();
            }
        }
        while (x != x1) {
            oneStep();
        }

        if (y < y1) {
            while (direction != Direction.UP) {
                toRotationRight();
            }
        } else if (y > y1) {
            while (direction != Direction.DOWN) {
                toRotationLeft();
            }
        }
        while (y != y1) {
            oneStep();
        }

    }


}
